import React, { useState } from 'react'
import { ReactQueryDevtools } from 'react-query/devtools'

import People from './components/People'
import Planets from './components/Planets'
import NavBar from './components/NavBar'
import './index.css'

function App() {
  const [ page, setPage ] = useState('planets')
  return (
    <div>
    <div className='App'>
      <h1>Star Wars Info</h1>
      <NavBar setPage={setPage} />
      <div className='content'>
        { page === 'planets' ? <Planets /> : <People /> }
      </div>
    </div>
      <ReactQueryDevtools initialIsOpen={false} />
    </div>
  );
}

export default App;
