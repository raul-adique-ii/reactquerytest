import React, { useState } from 'react'
// import { useQuery } from 'react-query'
import { useInfiniteQuery } from 'react-query'


import Planet from './Planet'



const Planets = () => {
    const [ page, setPage ] = useState(1)

    const fetchPlanets = async ( page ) => {
        const response = await fetch(`http://swapi.dev/api/planets/?page=${page}`)
    
        return response.json()
    }

    // const { 
    //     data,
    //     error,
    //     isFetching,
    //     isLoading,
    //     isError,  
    //     isPreviousData,
    // } = useQuery(['planets', page], () => fetchPlanets(page), { keepPreviousData: true })
    
    const { 
        data,
        error,
        fetchNextPage,
        hasNextPage,
        isFetching,
        isFetchingNextPage,
        status,
    } = useInfiniteQuery('planets', () => fetchPlanets(page), {
        getNextPageParam: (lastPage, allPages) => lastPage.nextCursor,
    })
    console.log(data);
    return (
        <div>
            <h2>Planets</h2>
            {/* <p>{ status }</p> */}

            {status === 'loading' ? (
                <p>Loading...</p>
            ) : status === 'error' ? (
                <p>Error: {error.message}</p>
            ) : (
                <>
                    {data.pages.map((group, i) => (
                        <React.Fragment key={i}>
                            {group.results.map(planet => <Planet key={planet.name} planet={planet} />)}
                        </React.Fragment>
                    ))}
                    <div>
                        <button onClick={() => setPage(old => Math.max(old -1, 1))}>
                            Previous Page
                        </button>
                        <span>{ page }</span>
                        <button onClick={() => setPage(old => (!hasNextPage ? old : hasNextPage))}>Next Page</button>



                        {/* <button
                            onClick={() => fetchNextPage()}
                            disabled={!hasNextPage || isFetchingNextPage}
                        >
                            {isFetchingNextPage
                                ? 'Loading More...'
                                : hasNextPage
                                ? 'Load More'
                                : 'Nothing more to load'
                            }
                        </button> */}
                    </div>
                    {/* <div>{isFetching && !isFetchingNextPage ? 'Fetching...' : null}</div> */}
                </>
            )}






            {/* {isLoading ? (
                <div>Loading...</div>
            ) : isError ? (
                <div>Error: {error.message}</div>
            ) : (
                <div>
                    <div>
                        {data.results.map(planet => <Planet key={planet.name} planet={planet} />)}
                    </div>
                </div>
            )}
            <span>Current Page: {page}</span>
            <button
                onClick={() => setPage(old => Math.max(old - 1, 1))}
                disabled={page === 0}
            >
                Previous Page
            </button>{' '}
            <button
                onClick={() => setPage(old => (isPreviousData ? old : old + 1))}
                disabled={isPreviousData}
            >
                Next Page
            </button>
                {isFetching ? <span>Loading...</span> : null}{' '} */}





            {/* <button onClick={() => setPage(1)}>Page 1</button>
            <button onClick={() => setPage(2)}>Page 2</button>
            <button onClick={() => setPage(3)}>Page 3</button>

            {status === 'loading' && (
                <div>Loading...</div>
            )}

            {status === 'error' && (
                <div>Cannot connect to the server</div>
            )}

            {status === 'success' && (
                <div>
                    {data.results.map(planet => <Planet key={planet.name} planet={planet} />)}
                </div>
            )} */}
        </div>
    )
}

export default Planets
